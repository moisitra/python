N = int(input())
hour = N // 3600 % 24
min1 = N // 60 % 60 // 10
min2 = N // 60 % 60 % 10
sec1 = N % 60 // 10
sec2 = N % 60 % 10
print(hour, min1, sep=':', end='')
print(min2, end=':')
print(sec1, end='')
print(sec2, end='')
