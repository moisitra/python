# sort list in alphabet order
f = open('input.txt', 'r', encoding='utf-8')
result = open('output.txt', 'w', encoding='utf-8')
l = []
for line in f:
    l.append(line.strip().split())
l.sort()
for row in l:
    result.write(row[0] + ' ' + row[1] + ' ' + row[3] + '\n')
f.close()
result.close()
