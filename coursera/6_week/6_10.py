# keyboard
emptyValue1 = int(input())
nPress = list(map(int, input().split()))
emptyValue2 = int(input())
sequencePress = list(map(int, input().split()))
myList = [0] * emptyValue1
for i in range(len(sequencePress)):
    myList[sequencePress[i] - 1] += 1
for j in range(len(myList)):
    if myList[j] > nPress[j]:
        print("YES")
    else:
        print("NO")
