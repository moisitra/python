# olympiad result (winners)
f = open('input.txt', 'r', encoding='utf-8')
myList = []
winners = [[0, 0], [0, 0], [0, 0]]
for line in f:
    myList.append(line.strip().split())
for word in myList:
    if winners[int(word[2]) - 9][0] < int(word[3]):
        winners[int(word[2]) - 9][1] = 1
        winners[int(word[2]) - 9][0] = int(word[3])
    elif winners[int(word[2]) - 9][0] == int(word[3]):
        winners[int(word[2]) - 9][1] += 1
for list in winners:
    print(list[1], end=' ')
f.close()
