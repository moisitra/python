# counting sort
def CountSort(A):
    B = [0] * (max(A) + 1)
    for i in A:
        B[i] += 1
    for j in range(len(B)):
        print((str(j) + ' ') * B[j], end='')


myList = list(map(int, input().split()))
CountSort(myList)
