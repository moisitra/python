f = open('input.txt', 'r', encoding='utf-8')
l = []
cls = [[], [], []]
for line in f:
    l.append(line.strip().split())
for i in range(len(l)):
    if l[i][2] == '9':
        cls[0].append(int(l[i][3]))
    elif l[i][2] == '10':
        cls[1].append(int(l[i][3]))
    elif l[i][2] == '11':
        cls[2].append(int(l[i][3]))
for i in cls:
    print(sum(i) / len(i), end=' ')
f.close()
