# archive
s, n = map(int, input().split())
i, k, r = 0, 0, 0
l = []
for new in range(n):
    l.append(int(input()))
l.sort()
while i < len(l) and k < s:
    if (k + l[i]) <= s:
        k += l[i]
        r += 1
    i += 1
print(r)
