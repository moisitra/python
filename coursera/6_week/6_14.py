# passing score
f = open('input.txt', 'r', encoding='utf-8')
k = int(f.readline().strip())
myList = []
score = [0] * 301
for line in f:
    myList.append(line.strip().split()[-1:-4:-1])
f.close()
# если баллы == индекс списка, увеличиваем на 1
for i in myList:
    if (int(i[0]) > 39) and (int(i[1]) > 39) and (int(i[2]) > 39):
        score[(int(i[0]) + int(i[1]) + int(i[2]))] += 1
out = open('output.txt', 'w', encoding='utf-8')
last = '1'
j = 300
if sum(score) == 0:
    out.write('1')
elif sum(score) <= k:
    out.write('0')
else:
    while j > 119:
        if score[j] >= 1 and k >= score[j]:
            k -= score[j]
            last = j
        elif score[j] >= k:
            break
        j -= 1
    out.write(str(last))
out.close()
