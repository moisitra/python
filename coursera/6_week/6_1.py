# merge sort
def merge(a, b):
    l = []
    i, j = 0, 0
    while i < len(a) and j < len(b):
        if a[i] < b[j]:
            l.append(a[i])
            i += 1
        else:
            l.append(b[j])
            j += 1
    l += a[i:] + b[j:]
    return l


a = list(map(int, input().split()))
b = list(map(int, input().split()))
print(*merge(a, b))
