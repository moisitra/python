# defence v_2
n = int(input())
n_l = list(map(int, input().split()))
for i in range(len(n_l)):
    n_l[i] = [n_l[i], i + 1]
n_l.sort()
m = int(input())
m_l = list(map(int, input().split()))
for i in range(len(m_l)):
    m_l[i] = [m_l[i], i + 1]
m_l = sorted(m_l, reverse=True)
j = 0
k = len(m_l) - 1
while j < len(n_l):
    if abs(n_l[j][0] - m_l[k][0]) < abs(n_l[j][0] - m_l[k - 1][0]):
        n_l[j].append(m_l[k][1])
        j += 1
    elif abs(n_l[j][0] - m_l[k][0]) > abs(n_l[j][0] - m_l[k - 1][0]) and k > 0:
        k -= 1
    else:
        n_l[j].append(m_l[k][1])
        j += 1
n_l.sort(key=lambda f: f[1])
for i in n_l:
    print(i[2], end=' ')
