# passing score
f = open('input.txt', 'r', encoding='utf-8')
myList = []
score = []
k = int(f.readline().strip())
for line in f:
    myList.append(line.strip().split()[-1:-4:-1])
f.close()
i = 0
while i < len(myList):
    if (int(myList[i][0]) < 40 or
            int(myList[i][1]) < 40 or int(myList[i][2]) < 40):
        myList.remove(myList[i])
    else:
        score.append(int(myList[i][0]) + int(myList[i][1]) + int(myList[i][2]))
        i += 1
j = 0
out = open('output.txt', 'w', encoding='utf-8')
if len(score) <= k:
    out.write('0')
else:
    score.sort()
    if score[-k] == score[-k - 1]:
        while score[-k] == score[-k + j] and (-k + j) < 0:
            j += 1
        if j == k:
            out.write('1')
        else:
            out.write(str(score[-k + j]))
    else:
        out.write(str(score[-k + j]))
out.close()
