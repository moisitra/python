# quadratic equation - 2
a = float(input())
b = float(input())
c = float(input())
if a == 0 and c == 0:
    print("3")
else:
    d = b ** 2 - 4 * a * c
    if d < 0 or (a == 0 and b == 0):
        print("0")
    elif a == 0:
        print("1", - c / b, sep=' ')
    else:
        x1 = (-b + d**(1/2)) / (2 * a)
        x2 = (-b - d**(1 / 2)) / (2 * a)
        if x1 == x2:
            print("1", x1, sep=' ')
        else:
            if x1 > x2:
                print("2", x2, x1, sep=' ')
            else:
                print("2", x1, x2, sep=' ')
