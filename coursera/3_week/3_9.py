# standart deviation
a = int(input())
i = 0
s1 = a
sq = a**2
while a != 0:
    a = int(input())
    i += 1
    s1 += a
    sq += a**2
s = s1 / i
sig = ((sq - 2 * s * s1 + i * s**2) / (i - 1))**(1/2)
print(sig)
