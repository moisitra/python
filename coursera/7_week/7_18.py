# word frequencies
import sys
text = sys.stdin.read()
didi = {}
for i in text.split():
    didi[i] = didi.get(i, 0) + 1
lit = [(didi[key], key) for key in didi]
lit = sorted(lit, key=lambda data: (-data[0], data[1]))
for i in lit:
    print(i[1])
