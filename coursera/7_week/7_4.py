# did the number occur before
lst = list(map(int, input().split()))
st = set()
for i in lst:
    if i in st:
        print("YES")
    else:
        st.add(i)
        print("NO")
