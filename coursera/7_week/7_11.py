# strikes
n, k = map(int, input().split())
day = [int(i) for i in range(1, n + 1) if i % 7 and (i % ((i // 7) * 7 + 6))]
strikeday = set()
for i in range(k):
    start, step = map(int, input().split())
    tempset = set(range(start, n + 1, step))
    strikeday = strikeday | tempset
print(len(strikeday & set(day)))
