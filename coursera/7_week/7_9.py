# language
listset = []
n = int(input())
for i in range(n):
    n2 = int(input())
    timeset = set([str(input()) for j in range(n2)])
    listset.append(timeset)
commonset = listset[0]
allset = listset[0]
for k in range(len(listset)):
    allset = allset | listset[k]
    commonset = commonset & listset[k]
print(len(commonset))
print(*commonset, sep='\n')
print(len(allset))
print(*allset, sep='\n')
