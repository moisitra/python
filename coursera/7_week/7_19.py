# president elections
f_in = open('input.txt', encoding='utf-8')
f_out = open('output.txt', 'w', encoding='utf-8')
s_um = 0
didi = {}
for i in f_in:
    didi[i.strip()] = didi.get(i.strip(), 0) + 1
f_in.close()
lit = [(didi[key], key) for key in didi]
lit.sort(reverse=True)
for key in didi:
    s_um += didi[key]
if len(lit) == 1:
    f_out.write(lit[0][1])
elif (lit[0][0] / s_um) > 1 / 2:
    f_out.write(lit[0][1])
else:
    f_out.write(f'{lit[0][1]}\n{lit[1][1]}')
f_out.close()
