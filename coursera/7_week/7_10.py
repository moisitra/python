# crossing bus routs
s1, f1, s2, f2 = map(int, input().split())
if f1 < s1:
    f1, s1 = s1, f1
if f2 < s2:
    f2, s2 = s2, f2
bro1 = set([int(i) for i in range(s1, f1 + 1)])
bro2 = set([int(i) for i in range(s2, f2 + 1)])
print(len(bro1 & bro2))
