# guess the number
n = int(input())
myset = set([int(i) for i in range(1, n + 1)])
tempset = set([i for i in input().split()])
while 'HELP' not in tempset:
    word = str(input())
    tempset = set([int(i) for i in tempset])
    if word == 'YES':
        myset &= tempset
    else:
        myset -= tempset
    tempset = set([i for i in input().split()])
print(*sorted(myset))
