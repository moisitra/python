# dict sinonims
n = int(input())
wordDict1 = {}
wordDict2 = {}
for i in range(n):
    k, j = input().split()
    wordDict1[k] = j
    wordDict2[j] = k
word = input()
if word in wordDict2:
    print(wordDict2[word])
else:
    print(wordDict1[word])
