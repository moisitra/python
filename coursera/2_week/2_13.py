# type of rectangle
a = int(input())
b = int(input())
c = int(input())
great = a
if b >= a or c >= a:
    great = b
    if c >= b and c >= a:
        great = c
if 2 * great >= a + b + c:
    print("impossible")
else:
    if 2 * great**2 == a**2 + b**2 + c**2:
        print("rectangular")
    elif 2 * great**2 > a**2 + b**2 + c**2:
        print("obtuse")
    elif 2 * great**2 < a**2 + b**2 + c**2:
        print("acute")
