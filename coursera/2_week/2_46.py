# The maximum length of the monotonous fragment
a = int(input())
p1 = a
p2 = a
i = 1
l = 1
while a != 0:
    a = int(input())
    if (a < p1 and p1 > p2) and a != 0:
        i = 1
    elif (a > p1 and p1 < p2) and a != 0:
        i = 1
    elif a == p1:
        i = 0
    if a != 0:
        i += 1
    if i > l:
        l = i
    p2 = p1
    p1 = a
print(l)
