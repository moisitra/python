# boxes with laptop
l1 = int(input())
h1 = int(input())
d1 = int(input())
l2 = int(input())
h2 = int(input())
d2 = int(input())

max1 = 0

count = (l1 // l2) * (h1 // h2) * (d1 // d2)
max1 = count

count = (l1 // l2) * (h1 // d2) * (d1 // h2)
if count > max1:
    max1 = count

count = (l1 // h2) * (h1 // d2) * (d1 // l2)
if count > max1:
    max1 = count

count = (l1 // h2) * (h1 // l2) * (d1 // d2)
if count > max1:
    max1 = count

count = (l1 // d2) * (h1 // l2) * (d1 // h2)
if count > max1:
    max1 = count

count = (l1 // d2) * (h1 // h2) * (d1 // l2)
if count > max1:
    max1 = count

print(max1)
