# The maximum number of consecutive equal
a = int(input())
b = a
i = 1
s = 0
while a != 0:
    a = int(input())
    if a == b:
        i += 1
    else:
        i = 1
    if i > s:
        s = i
    b = a
print(s)
