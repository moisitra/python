# number of elements equal to maximum
a = int(input())
b = a
i = 1
while a != 0:
    a = int(input())
    if a == b:
        i += 1
    if a > b:
        b = a
        i = 1
print(i)
