# reverse
n = int(input())
k = 1
while n > 9:
    print(n % 10, end='')
    n = (n - (n % 10)) // 10
    k *= 10
print(n, sep='')
