# cutlets
k = int(input())
m = int(input())
n = int(input())
if n <= k:
    print(2 * m)
elif n % k == 0:
    print((n // k) * m * 2)
else:
    print((n * 2 + k - 1) // k * m)
