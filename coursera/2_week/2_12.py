# checkers
a1 = int(input())
b1 = int(input())
a2 = int(input())
b2 = int(input())
if (((a2 - a1) > (b2 - b1)) or
        ((a1 + b1) % 2 != (a2 + b2) % 2) or
        ((a1 + b1) > (a2 + b2))):
    print("NO")
else:
    print("YES")
