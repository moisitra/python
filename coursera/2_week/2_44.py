# palindrome
n = int(input())
i = 1
s = 0
while i <= n:
    x = i
    a = ''
    while x > 0:
        a += str(x % 10)
        x //= 10
    if i == int(a):
        s += 1
    i += 1
print(s)
