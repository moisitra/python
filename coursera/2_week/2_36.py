# the number of even elements of the sequence
a = int(input())
i = 0
while a != 0:
    if a % 2 == 0:
        i += 1
    a = int(input())
print(i)
