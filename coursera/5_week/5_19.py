# neighbors of the same sign
l = list(map(int, input().split()))
a1 = l[0]
a2 = l[1]
for i in range(1, len(l)):
    if l[i] / l[i - 1] > 0:
        print(l[i - 1], l[i], sep=' ')
        break
