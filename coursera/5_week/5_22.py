# lower positive elements
l = sorted(list(map(int, input().split())), reverse=True)
s = l[0]
for i in range(1, len(l)):
    if 0 < l[i] < s:
        s = l[i]
print(s)
