# compressing list
l = list(map(int, input().split()))
for i in range(len(l) - 1, -1, -1):
    if l[i] == 0:
        l.append(l.pop(i))
print(*l)
