# the greater element
l = list(map(int, input().split()))
s = l[0]
k = 0
for i in range(1, len(l)):
    if l[i] > s:
        s = l[i]
        k = i
print(s, k, sep=' ')
