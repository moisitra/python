# number of matching pairs
l = list(map(int, input().split()))
k = 0
for i in range(len(l)):
    for j in range(i + 1, len(l)):
        if l[i] == l[j]:
            k += 1
print(k)
