# greater then neighbors
l = list(map(int, input().split()))
s = 0
for i in range(1, len(l) - 1):
    if l[i - 1] < l[i] > l[i + 1]:
        s += 1
print(s)
