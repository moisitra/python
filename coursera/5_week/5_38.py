# kegelban
n, k = map(int, input().split())
li = list(n * 'I')
for i in range(1, k + 1):
    l, r = map(int, input().split())
    for j in range(l - 1, r):
        li[j] = '.'
print(''.join(li))
