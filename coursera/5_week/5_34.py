# Greatest product of two numbers
l = list(map(int, input().split()))
p_m1 = 0
p_m2 = 0
n_m1 = 0
n_m2 = 0
for i in range(len(l)):
    if l[i] >= p_m2:
        if l[i] >= p_m1:
            p_m2 = p_m1
            p_m1 = l[i]
            continue
        p_m2 = l[i]
    if l[i] <= n_m2:
        if l[i] <= n_m1:
            n_m2 = n_m1
            n_m1 = l[i]
            continue
        n_m2 = l[i]
if p_m1 * p_m2 > n_m1 * n_m2:
    print(p_m2, p_m1, sep=' ')
else:
    print(n_m1, n_m2, sep=' ')
