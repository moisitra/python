# lower odd element
l = list(map(int, input().split()))
s = True
for i in range(len(l)):
    if l[i] % 2 != 0:
        if s:
            s = False
            k = l[i]
        elif l[i] < k:
            k = l[i]
print(k)
