# ascending list?
def IsAscending(a):
    t = ['NO', 'YES']
    i = 1
    while len(a) > 1 and i < (len(a)) and a[i] > a[i - 1]:
        i += 1
    return t[0**(len(a) - i)]


l = list(map(int, input().split()))
print(IsAscending(l))
