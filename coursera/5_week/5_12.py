# wonderful numbers
a = int(input())
b = int(input())
for i in range(a, b + 1):
    k = tuple(str(i))
    if k[0] == k[-1] and k[1] == k[-2]:
        print(i)
