# min vs max
l = list(map(int, input().split()))
k_mi = 0
k_ma = 0
for i in range(len(l)):
    if l[i] > l[k_ma]:
        k_ma = i
    if l[i] < l[k_mi]:
        k_mi = i
l[k_ma], l[k_mi] = l[k_mi], l[k_ma]
print(' '.join(map(str, l)))
