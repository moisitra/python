# number of different elements
l = list(map(int, input().split()))
k = 1
i = 1
while i < len(l):
    if l[i] != l[i - 1]:
        k += 1
    i += 1
print(k)
