# number of positive elements
s = list(map(int, input().split()))
r = 0
for i in s:
    if i > 0:
        r += 1
print(r)
