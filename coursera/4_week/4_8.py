# XOR
def xor(x, y):
    return not(x**y and y**x)


x = int(input())
y = int(input())
print(int(xor(x, y)))
