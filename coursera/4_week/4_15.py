# euclid algorithm
def gcd(a, b):
    if a == b or b == 0:
        return a
    return gcd(b, a % b)


a = int(input())
b = int(input())
print(gcd(a, b))
