# faster exponential
def fexp(a, n):
    if n == 0:
        return 1
    if n == -1:
        return 1 / a
    s = fexp(a, n // 2)
    s *= s
    if n % 2:
        s *= a
    return s

a = float(input())
n = int(input())
print(fexp(a, n))
