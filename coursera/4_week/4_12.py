# exponentiation
def power(a, n):
    if n < 0:
        i = -1
        s = 1 / a
        while i > n:
            s *= (1 / a)
            i -= 1
        return s
    if n > 0:
        s = a
        i = 1
        while i < n:
            s *= a
            i += 1
        return s
    return 1

a = float(input())
n = int(input())
print(power(a, n))
