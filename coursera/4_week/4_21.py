# hanoi tower
def move(n, x, y):
    if n == 0:
        return
    ms = 6 - x - y
    move(n - 1, y, ms)
    print(n, x, y)
    move(n - 1, ms, y)
    return


n = int(input())
move(n, 1, 3)
