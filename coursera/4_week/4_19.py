# recursion sum of numbers
def rsum():
    a = int(input())
    if a == 0:
        return a
    return a + rsum()


print(rsum())
