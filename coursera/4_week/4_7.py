# is point in the difficult figure
def IsPointInArea(x, y):
    return (((x + 1)**2 + (y - 1)**2) <= 4 and (x >= -y) and
            (x <= ((y - 2) / 2))) or (((x + 1)**2 + (y - 1)**2) >= 4 and
                                      (x <= -y) and (x >= ((y - 2) / 2)))


x = float(input())
y = float(input())
if IsPointInArea(x, y):
    print("YES")
else:
    print("NO")
