# fraction reduction
def gcd(a, b):
    if a == b or b == 0:
        return (int(a))
    return gcd(b, a % b)


def ReduceFraction(n, m):
        return n // gcd(n, m), m // gcd(n, m)


n = int(input())
m = int(input())
print(*ReduceFraction(n, m))
