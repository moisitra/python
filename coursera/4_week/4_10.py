# prime number
def IsPrime(n):
    i = 2
    while i <= n**(1 / 2):
        if n % i == 0:
            return "NO"
        i += 1
    return "YES"


n = int(input())
print(IsPrime(n))
